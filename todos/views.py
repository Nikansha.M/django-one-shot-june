from audioop import reverse
from django.urls import reverse_lazy

from todos.models import TodoList, TodoItem

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    
    # fields for the create a new list form
    # from TodoList model
    fields = ["name"]
    
    # Redirects to the detail page for the
    # instance just created
    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"

    # Redirects to the list view for the model
    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitem/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    
    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todoitem/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])
