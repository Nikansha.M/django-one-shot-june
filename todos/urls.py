from django.urls import path

from todos.views import (
    TodoListListView,
    TodoListDetailView,
    TodoListCreateView,
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView
)

urlpatterns = [
    
    # TODOLIST
    path("", TodoListListView.as_view(), name="list_todos"),
    # This <int:pk> means that you need to have an id for the url tag
    path("<int:pk>/", TodoListDetailView.as_view(), name="show_todolist"),
    path("create/", TodoListCreateView.as_view(), name="create_todolist"),
    # This <int:pk> means that you need to have an id for the url tag
    path(
        "<int:pk>/edit/",
        TodoListUpdateView.as_view(),
        name="update_todolist"),
    path(
        "<int:pk>/delete/",
        TodoListDeleteView.as_view(),
        name="delete_todolist"),
    
    # TODOITEM
    path(
        "items/create/",
        TodoItemCreateView.as_view(),
        name="create_todoitem"),
    path(
        "items/<int:pk>/edit/",
        TodoItemUpdateView.as_view(),
        name="update_todoitem"),
]