### Set Up Directory and GitLab Project

1. Cd into your projects directory
1. Fork and Clone the starter project to your Projects Directory
1. Since I already have a Django-One-Shot repository in GitLab, additional steps I need to  follow:
    1. Create a blank GitLab project without the readme file
    1. Git clone https://gitlab.com/sjp19-public-resources/django-one-shot.git (SJP Django-One-Shot)
    1. Cd django-one-shot
    1. Git remote rm origin (to remove the origin branch)
    1. Git remote add origin https://gitlab.com/Nikansha.M/django-one-shot-june.git (add the blank Django-One-Shot project I created on GitLab without the readme file
    1. Git remote -v
        1. This will list out:
            1. origin  https://gitlab.com/Nikansha.M/django-one-shot-june.git (fetch)
            1. origin  https://gitlab.com/Nikansha.M/django-one-shot-june.git (push)
    1. git push --set-upstream origin main (this is a one-time command you’ll use)

TO PUSH YOUR PROJECT UP TO GITLAB, USE **git push origin main**


### Set Up Commands

1. Create Virtual environment:  **python -m venv .venv**
1. Activate Virtual environment:  **./.venv/Scripts/Activate.ps1**
1. Upgrade pip:  **python -m pip install --upgrade pip**
1. Install django:  **pip install django**
1. Install black:  **pip install black**
1. Install flake8:  **python -m pip install flake8**
1. Install djhtml:  **pip install djhtml**
1. Deactivate your virtual environment:  **deactivate**
1. Activate your virtual environment:  **./.venv/Scripts/Activate.ps1**
1. Use pip freeze to generate a requirements.txt file:  **pip freeze > requirements.txt**


#### Create Django Project & App

1. Create the django project named brain_two:  
    1. django-admin startproject brain_two . (don’t forget the dot!!!)
1. Create the django app named todos:  
    1. python manage.py startapp todos
1. Run migrations:
    1. python manage.py makemigrations
    1. python manage.py migrate
1. Create a superuser:
    1. python manage.py createsuperuser
1. Install todos in the brain_two Django project settings.py file
